import org.junit.Test
import org.junit.Assert

class KotlinLibraryTest {
  [Test] public fun testSomeLibraryMethod() {
    val classUnderTest = Library()
    Assert.assertTrue("someLibraryMethod should return 'true'", classUnderTest.someLibraryMethod())
  }
}
